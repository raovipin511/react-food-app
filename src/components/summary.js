import axios from "axios";
import { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
function Summary() {
    var [cart, setCart] = useState([]);
    var totalprice = 0;

    useEffect(() => {
        axios({
            url: process.env.REACT_APP_BASE_URL+ "/cakecart",
            method: 'post',
            data: {
            },
        }).then((response) => {
            var allcart = response.data.data;
            setCart(allcart);
        }, (error) => {
            alert("Something went wrong try again later")
        })
    }, [])

    return (
        <>
            <div className="container">
                <table id="cart" className="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th style={{width:"60%"}}>Product</th>
                            <th style={{width:"10%"}}>Price</th>
                            <th style={{width:"10%"}}>Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            (cart).map((each, index)=>{
                                totalprice += each.price
                                return (
                                <tr key={index}>
                                    <td data-th="Product">
                                        <div className="row">
                                            <div className="col-sm-2 hidden-xs"><img src={each.image} alt="..." className="img-responsive" /></div>
                                            <div className="col-sm-10">
                                                <h4 className="nomargin">{each.name}</h4>
                                            </div>
                                        </div>
                                    </td>
                                    <td data-th="Price">${each.price}</td>
                                    <td data-th="Quantity">
                                    {each.quantity}
                                     </td>
                                </tr>
                                )
                            })
                        }
                    </tbody>
                    <tfoot>
                        <tr>
                            <td className="hidden-xs text-center"><strong>Total ${totalprice}</strong></td>
                            <td className="hidden-xs"></td>
                            <td><Link to="/checkout/address" className="btn btn-success btn-block">Next <i className="fa fa-angle-right"></i></Link></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </>
    );
}

export default Summary