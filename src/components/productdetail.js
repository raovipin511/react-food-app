import axios from "axios";
import { Link, useParams, withRouter } from 'react-router-dom';
import { useEffect, useState } from 'react';
import './custom.css';
import { connect } from "react-redux";
import { addcart } from "../reduxstore/middlewares";

function Productdetail(props) {
    var params = useParams()
    var apiUrl = process.env.REACT_APP_BASE_URL+ "/cake/" + params.proId;
    var [product, setProduct] = useState('');
    var [ingredients, setIngredients] = useState([]);

    useEffect(() => {
        axios({ 
            mehod: "get", 
            url: apiUrl 
        }).then((response) => {
            var productdata = response.data.data;
            setProduct(productdata)
            setIngredients(productdata.ingredients)
        }, (error) => { 
            alert('Something went wrong try again later')
        })
    }, [])


    let addtocart = () => {
        axios({
            url: process.env.REACT_APP_BASE_URL+ "/addcaketocart",
            method: 'post',
            data: { 
                "cakeid":product.cakeid,
                "name":product.name,
                "image":product.image,
                "price":product.price,
                "weight":product.weight,
            },
            headers:{
                "authtoken":localStorage.token,
            }
        }).then((response) => {
            props.dispatch({
                type:"ADDTOCART",
                payload:{
                    data:response.data.data
                }
            })
            props.history.push("/cart")
        }, (error) => {
            alert("Something went wrong try again later")
        })
    }

    return (
        <div className="container-fluid" style={{ marginTop: "20px" }}>
            <div className="container">
                <div className="card">
                    <div className="container-fliud">
                        <div className="wrapper row">
                            <div className="preview col-md-6">
                                <div className="preview-pic tab-content">
                                    <div className="tab-pane active" id="pic-1"><img src={product.image} /></div>
                                </div>
                            </div>
                            <div className="details col-md-6">
                                <h3 className="product-title">{product.name}</h3>
                                <div className="rating">
                                    <div className="stars">
                                        <span className="fa fa-star checked"></span>
                                        <span className="fa fa-star checked"></span>
                                        <span className="fa fa-star checked"></span>
                                        <span className="fa fa-star"></span>
                                        <span className="fa fa-star"></span>
                                    </div>
                                    <span className="review-no">{product.reviews} reviews</span>
                                </div>
                                <p className="product-description">{product.description}</p>
                                <h4 className="price">current price: <span>Rs {product.price}</span></h4>
                                <h4 className="price">flavour: <span>{product.flavour}</span></h4>
                                <h5 className="sizes">ingredients:
                                    {
                                        ingredients.map((each, index)=>{
                                            return <span className="size" data-toggle="tooltip" title={each} key={index}>{each}</span>
                                        }) 
                                    }
                                </h5>
                                <div className="action">
                                    {/* <Link to="/checkout"><button className="add-to-cart btn btn-default" type="button">checkout</button></Link>  */}
                                    <button onClick={addtocart} className="add-to-cart btn btn-default" type="button">Add to cart</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default connect()(withRouter(Productdetail))