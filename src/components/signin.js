import {Component} from "react";
import axios from "axios";
import { Link, withRouter } from "react-router-dom"
import { connect } from "react-redux";
import { loginmiddleware } from "../reduxstore/middlewares";

class Signin extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            errors: {
                email: '',
                password: ''
            }
        }
    }

    handleChange = (event) => {
        event.preventDefault();
        let filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const { name, value } = event.target;
        let errors = this.state.errors;

        switch (name) {
            case 'email':
                errors.email = filter.test(value)?'':'Email is not valid!';
                break;
            case 'password':
                errors.password = value.length < 1?'Password should not be empty':'';
                break;
            default:
                break;
        }

        this.setState({errors, [name]: value});
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.dispatch(loginmiddleware(this.state))
    }

    render() {
        const {errors} = this.state;
        return (
            <div className="container" style={{marginTop: "100px", marginBottom: "100px"}}>
                <div className="row">
                    <div className="col-6 text-left"><label>Sign in</label> </div>
                    <div className="col-6 text-right"><Link to="/signup" > <label>Sign up</label></Link></div>
                </div>
                <form onSubmit={this.handleSubmit} noValidate>
                    <div className="form-group email">
                        <input value={this.state.email} name='email' onChange={this.handleChange} className="form-control" placeholder="Enter Email ID"/>
                        {errors.email.length > 0 && <span className='error'>{errors.email}</span>}
                    </div>
                    <div className="form-group role">
                        <input type="password" value={this.state.password} name='password' onChange={this.handleChange} className="form-control" placeholder="Enter Password"/>
                        {errors.password.length > 0 && <span className='error'>{errors.password}</span>}
                    </div>
                    <button type="submit" className="form-control btn btn-primary">Sign In</button>
                </form>
            </div>
        )
    }
}

Signin = connect((state, props) => {
    if(state.AuthReducer?.isLoggedIn==true){
        props.history.push("/")
    }else{
        return {
            isloading:state.AuthReducer?.isLoading
        }
    }
})(Signin)

export default withRouter(Signin);