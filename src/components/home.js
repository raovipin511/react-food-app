import Carousel from './carousel';
import Cardlist from './cardlist';

function Home() {
  return (
      <>
        <Carousel></Carousel>
        <Cardlist></Cardlist>
      </>
    );
  }

  export default Home;