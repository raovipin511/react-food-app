import Card from './card';
import axios from "axios";
import { useEffect, useState } from 'react';
import querystring from "query-string"

function Search(props) {
    var query = querystring.parse(props.location.search)
    var apiUrl = process.env.REACT_APP_BASE_URL+ "/searchcakes?q=" + query.q;
    var [cake, setCake] = useState([]);

    useEffect(() => {
        axios({
            mehod: "get",
            url: apiUrl
        }).then((response) => {
            var allcake = response.data.data;
            setCake(allcake);
        }, (error) => {

        })
    }, [query.q])

    if (cake.length > 0) {
        return (
            <div className="container-fluid" style={{ marginTop: "20px" }}>
                <h2 className="text-center mt-5 mb-5"> Product Search for {query.q} </h2>
                <div className="row">
                    {
                        cake.map((each, index) => {
                            return <Card product={each} key={index}></Card>
                        })
                    }
                </div>
            </div>
        );
    } else {
        return (
            <div className="container-fluid" style={{ marginTop: "20px" }}>
                <h2 className="text-center mt-5 mb-5"> Product Search for {query.q} </h2>
                <div className="row text-center">
                    <h2 className="text-center mt-5 mb-5" >No result found</h2>
                </div>
            </div>
        );
    }
}

export default Search