import { Link } from "react-router-dom"

function Card(props) {
    var url = "/product/" + props.product.cakeid

    return (
        <div className="card m-4">
            <Link to={url}><div className="hover01 column"><figure><img className="card-img-top" src={props.product.image} alt="Card cap" style={{height:"320px", width:"316px"}}/></figure></div></Link>
            <div className="card-body">
                <h5 className="card-title">{props.product.name.length>27?props.product.name.substring(1, 27)+'..':props.product.name}</h5>
                <p className="card-text"><small className="text-muted">Rs {props.product.price}</small></p>
            </div>
        </div>
    );
}

export default Card