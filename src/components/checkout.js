import { Link, Redirect, Route, useRouteMatch } from 'react-router-dom';
import Address from './address';
import Confirm from './confirm';
import Summary from './summary';

function Checkout() {
    var { path } = useRouteMatch()

    return (
        <>
            <div className="container-fluid" style={{ margin: "100px" }}>
                <div className="row">
                    <div className="col-4">
                        <div className="container-fluid p-5">
                            <h4>Checkout</h4>
                            <hr />
                            <Link to={`${path}/summary`}><p><span className="price">Summary</span></p></Link>
                            <Link to={`${path}/address`}><p><span className="price">Address</span></p></Link>
                            {/* <Link to={`${path}/confirm`}><p><span className="price">Confirmation</span></p></Link> */}
                        </div>
                    </div>
                    <div className="col-6">
                        <div className="container-fluid p-5">
                            <Route exact path={path}><Redirect to={`${path}/summary`}></Redirect></Route>
                            <Route exact path={`${path}/summary`} component={Summary}></Route>
                            <Route exact path={`${path}/address`} component={Address}></Route>
                            {/* <Route exact path={`${path}/confirm`} component={Confirm}></Route> */}
                        </div>
                    </div>
                </div>
            </div>
            
        </>
    );
}

export default Checkout