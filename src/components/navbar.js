import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom"

function Navbar(props) {

    let searchString = ""

    let search = (event) => {
        event.preventDefault()
        if (searchString) {
            var url = "/search?q=" + searchString;
            props.history.push(url)
        }
    }

    let getSearchText = function (event) {
        searchString = event.target.value
    }

    let logout = () => {
        props.dispatch({
            type: "LOGOUT",
            payload: {
                token: localStorage.token
            }
        })
        props.history.push('/')
    }

    return (
        <nav className="navbar navbar-expand navbar-dark bg-dark">
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
            <Link to="/"><p className="navbar-brand" >Food App</p></Link>
            <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
            </ul>
            <form className="form-inline my-2 my-lg-0 text-center">
            <input onChange={getSearchText} className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
            <button onClick={search} className="btn btn-outline-success my-2 my-sm-0">Search</button>
            </form>
            { props.isLoggedIn && <Link to="/cart"><div id="ex3"><span className="p1 fa-stack fa-2x has-badge" data-count={props.cart.length}> <i className="p3 fa fa-shopping-cart fa-stack-1x fa-inverse"></i></span></div></Link>}
            { props.isLoggedIn && (localStorage.username=='Vipin Yadav' || localStorage.username=='Ashu Lekhi') && <Link to="/admin/cake"><button className="btn btn-outline-primary my-2 my-sm-0 ml-2" type="button"><i className="fa fa-user"></i> Admin</button></Link>}
            { props.isLoggedIn && <Link to="/orders"><button className="btn btn-outline-warning my-2 my-sm-0 ml-2" type="button">Orders</button></Link>}
            { !props.isLoggedIn && <Link to="/signin"><button className="btn btn-outline-success my-2 my-sm-0 ml-2" type="button">Signin</button></Link> }
            { props.isLoggedIn && <button onClick={logout} className="btn btn-outline-danger my-2 my-sm-0 ml-2" type="button">Logout</button> }
            
            {/* <Link to="/signup"><button className="btn btn-outline-success my-2 my-sm-0 ml-2" type="button">signup</button></Link> */}
        </div>
    </nav>
    )
}



export default connect((state) => {
    return {
        username: state.AuthReducer.username,
        email: state.AuthReducer.email,
        isLoggedIn: state.AuthReducer.isLoggedIn,
        cart:state.CartReducer.cart
    }
})(withRouter(Navbar))