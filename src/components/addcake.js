import axios from "axios"
import { useState } from "react"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"


function AddCake(props) {

    var [cakeName, setCakeName] = useState('')
    var [cakeDesc, setCakeDesc] = useState('')
    var [cakePrice, setCakePrice] = useState('')
    var [cakeFlav, setCakeFlav] = useState('')
    var [cakeType, setCakeType] = useState('')
    var [cakeWeight, setCakeWeight] = useState('')
    var [cakeEggless, setCakeEggless] = useState(false)
    var [cakeImage, setCakeImage] = useState('')
    var [cakeIngredients, setCakeIngredients] = useState([{ value: '' }])

    var cakeIngredientsElements = []

    function handleValidations() {
        return cakeImage !== '' && cakeName !== '' && cakePrice !== '' && cakeType !== '' && cakeFlav !== '' && cakeWeight !== '';
    }

    const handlePlusClick = (e) => {
        setCakeIngredients([...cakeIngredients, { value: '' }]);
    };

    const handleMinusClick = (index) => {
        const list = [...cakeIngredients];
        list.splice(index, 1);
        setCakeIngredients(list);

    };
    const handleInputChange = (event, index) => {
        event.preventDefault()
        const list = [...cakeIngredients];
        list[index].value = event.target.value;
        setCakeIngredients(list);
    }

    const handleOnSubmit = (event) => {
        event.preventDefault();
        cakeIngredients.map((each, index) => {
            cakeIngredientsElements.push(each.value)
        })

        props.dispatch({
            type: 'ADD_CAKE',
            payload: {
                name: cakeName, description: cakeDesc, price: cakePrice, weight: cakeWeight, image: cakeImage,
                type: cakeType, eggless: cakeEggless, flavour: cakeFlav, ingredients: cakeIngredientsElements, history: props
            }
        })

        cakeIngredientsElements = []
    }

    const fileUpload = (event) => {
        setCakeImage(URL.createObjectURL(event.target.files[0]))
        let formData = new FormData()
        formData.append('file', event.target.files[0])
        axios({
            url: process.env.REACT_APP_BASE_URL + '/upload',
            method: 'post',
            data: formData
        }).then(res => {
            setCakeImage(res.data.imageUrl)
        }, err => { })
    }

    return (
        <>
            <div className="container">
                <strong style={{ fontSize: "30px" }}>Add Cake</strong>
                <hr />
                <form onSubmit={handleOnSubmit}>
                    <div className="row" >
                        <label for="formFile">Image Upload</label>
                        <input className="form-control" onChange={fileUpload} type="file" id="formFile" />
                        {cakeImage &&
                            <img src={cakeImage} height="100px" width="100px" />
                        }
                    </div>
                    <div className="row">
                        <label for="cake_name">Cake Name</label>
                        <input type="text" readonly className="form-control" id="cake_name" onChange={(e) => { setCakeName(e.target.value) }} value={cakeName} />
                    </div>
                    <div className="row">
                        <label for="cake_desc">Cake Description</label>
                        <input type="text" className="form-control" id="cake_desc" onChange={(e) => { setCakeDesc(e.target.value) }} value={cakeDesc} />
                    </div>
                    <div className="row">
                        <label for="price">Price</label>
                        <input type="number" className="form-control" id="price" value={cakePrice} onChange={(e) => { setCakePrice(e.target.value) }} />
                    </div>
                    <div className="row">
                        <label for="weight">Weight</label>
                        <input type="number" className="form-control" id="weight" value={cakeWeight} onChange={(e) => { setCakeWeight(e.target.value) }} />
                    </div>
                    <div className="row">
                        <label for="type">Type</label>
                        <select className="form-control" value={cakeType} onChange={(e) => { setCakeType(e.target.value) }}>
                            <option value="" disabled>Select Type</option>
                            <option value="birthday">Birthday</option>
                            <option value="anniversary">Anniversary</option>
                            <option value="farewell">Farewell</option>
                        </select>
                    </div>
                    <div className="row">
                        <label for="check" >EggLess
                            <input className="form-control" type="checkbox" value={cakeEggless} onChange={(e) => { setCakeEggless(true) }} id="check" />
                        </label>
                    </div>
                    <div className="row">
                        <label for="flavour">Flavour</label>
                        <input type="text" className="form-control" id="flavour" value={cakeFlav} onChange={(e) => { setCakeFlav(e.target.value) }} />
                    </div>
                    <br />

                    <div className="row">
                        <button className="form-control btn btn-primary" disabled={!handleValidations()}>Submit form</button>
                    </div>
                </form>
            </div>

        </>
    )
}

export default connect()(withRouter(AddCake))