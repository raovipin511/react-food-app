import axios from "axios";
import { useEffect, useState } from 'react';
import { connect } from "react-redux";
import { Link, withRouter } from 'react-router-dom';

function Admin() {
    var [cake, setCake] = useState([]);

    useEffect(() => {
        axios({
            url: process.env.REACT_APP_BASE_URL + "/allcakes",
            method: 'get',
            data: {
            },
        }).then((response) => {
            var allcart = response.data.data;
            setCake(allcart);
        }, (error) => {
            alert("Something went wrong try again later")
        })
    }, [])
    return (
        <>
            <div className="container">
                <div className="row text-right">
                    <Link to="/admin/addcake"><button className="btn btn-success pull-right">+ Add Cake</button></Link>
                </div>
                <table id="cart" className="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th style={{ width: "50%" }}>Product</th>
                            <th style={{ width: "10%" }}>Price</th>
                            <th style={{ width: "10%", textAlign: "center" }}>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            (cake).map((each, index) => {
                                return (
                                    <tr key={index}>
                                        <td data-th="Product">
                                            <div className="row">
                                                <div className="col-sm-2 hidden-xs"><img src={each.image} alt="..." className="img-responsive" /></div>
                                                <div className="col-sm-10">
                                                    <h4 className="nomargin">{each.name}</h4>
                                                </div>
                                            </div>
                                        </td>
                                        <td data-th="Price">${each.price}</td>
                                        <td className="actions" data-th="">
                                            <button className="btn btn-danger btn-sm"><i className="fa fa-pencil"></i></button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        </>
    )
}

export default withRouter(Admin)