import axios from "axios"
import { useEffect, useState } from "react"
import { connect } from "react-redux"
import { Link, withRouter } from "react-router-dom"

function Address(props) {
    var [price, setPrice] = useState('')
    var [address, setAddress] = useState('')
    var [city, setCity] = useState('')
    var [pincode, setPincode] = useState('')
    var [phone, setPhone] = useState('')
    var [cakes, setCakes] = useState('')

    useEffect(() => {
        axios({
            url: process.env.REACT_APP_BASE_URL+ "/cakecart",
            method: 'post',
            data: {},
        }).then((response) => {
            var allcart = response.data.data;
            setCakes(allcart);
        }, (error) => {
            alert("Something went wrong try again later")
        })
    }, [])

    var getAddress = (event) => {
        setAddress(event.target.value);
    }
    var getCity = (event) => {
        setCity(event.target.value);
    }
    var getPincode = (event) => {
        setPincode(event.target.value);
    }
    var getPhone = (event) => {
        setPhone(event.target.value);
    }

    var formSubmit = (event) => {
        event.preventDefault();
        axios({
            url: process.env.REACT_APP_BASE_URL+ "/addcakeorder",
            method: 'post',
            data: {
                name:props.name, 
                price: 1000, 
                address: address,
                city: city,
                pincode: pincode,
                phone: phone,
                cakes: cakes
            }
        }).then(res => {
            if(res.messageg=="order placed"){
                props.dispatch({
                    type: 'PLACEORDER',
                    payload: {
                        data: res.data
                    }
                })
                props.history.push('/orders')
            }
        }, err => {})
    }

    return (
        <>
            <div className="container">
                <form onSubmit={formSubmit}>
                    <div className="form-group address">
                        <input value={address} name='address' onChange={getAddress} className="form-control" placeholder="Enter Address" />
                    </div>
                    <div className="form-group city">
                        <input value={city} name='city' onChange={getCity} className="form-control" placeholder="Enter City" />
                    </div>
                    <div className="form-group pincode">
                        <input value={pincode} name='pincode' onChange={getPincode} className="form-control" placeholder="Enter Pin Code" />
                    </div>
                    <div className="form-group phone">
                        <input value={phone} name='phone' onChange={getPhone} className="form-control" placeholder="Enter Phone" />
                    </div>
                    <button className="btn btn-success btn-block">Place Order <i className="fa fa-angle-right"></i></button>
                </form>
            </div>
        </>
    );
}

export default connect((state) => {
    return {
        name: state.AuthReducer.username,
    }
})(withRouter(Address))