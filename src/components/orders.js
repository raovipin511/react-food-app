import { useEffect, useState } from "react";
import axios from "axios";

const Orders = () => {
    var [orders, getOrders] = useState([])

    useEffect(() => {
        axios({
            url: process.env.REACT_APP_BASE_URL + '/cakeorders',
            method: 'post',
        }).then(res => {
            var list = res.data.cakeorders
            getOrders(list)
        }, err => { })
    }, [])

    return (
        <div className="container" style={{ marginTop: "100px" }}>
            <h1>Orders</h1>
            {orders.length > 0 &&
                <div className="container">
                    <table id="cart" className="table table-hover table-condensed">
                        <thead>
                            <tr>
                                <th style={{ width: "60%" }}>Order Id</th>
                                <th style={{ width: "60%" }}>Name</th>
                                <th style={{ width: "10%" }}>Address</th>
                                <th style={{ width: "10%" }}>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                orders.map((each, index) => {
                                    return (
                                        <tr key={index}>
                                            <td data-th="Price">{each.orderid}</td>
                                            <td data-th="Price">{each.name}</td>
                                            <td data-th="Price">{each.city}</td>
                                            <td data-th="Price">${each.price}</td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                </div>
                // <div className="accordion" id="accordionExample">
                //     {
                //         orders.map((each, index) => {
                //             return (
                //                 <>
                //                     <div className="card">
                //                         <div className="card-header" id={"heading" + (index)}>
                //                             <h2 className="mb-0">
                //                                 <button className="btn btn-link btn-block text-left collapsed" type="button"
                //                                     data-toggle="collapse" data-target={"#collapse" + (index)}
                //                                     aria-expanded="true" aria-controls={"collapse" + (index)}>
                //                                     Order Details #: {each.orderid}
                //                                 </button>
                //                             </h2>
                //                         </div>

                //                         <div id={"collapse" + (index)} className="collapse" aria-labelledby={"heading" + (index)}
                //                             data-parent="#accordionExample">
                //                             {
                //                                 each.cakes.map((eachCake, cakeIndex) => {
                //                                     return (
                //                                         <div className="row">
                //                                             <div className="card col-md-3">
                //                                                 <div className="card-body">
                //                                                     <img src={eachCake.image} className="card-img-top" alt="..." style={{ maxHeight: '213px', minHeight: '213px' }} />
                //                                                 </div>
                //                                             </div>
                //                                             <div className="col-md-8">
                //                                                 <h4>{eachCake.name}</h4>
                //                                                 <div style={{ textAlign: 'left' }}>
                //                                                     <strong>Name :</strong> {eachCake.name}
                //                                                 </div>
                //                                                 <div style={{ textAlign: 'left' }}>
                //                                                     <strong>Price :</strong> Rs. {eachCake.price} /-
                //                                                 </div>
                //                                                 <div style={{ textAlign: 'left' }}>
                //                                                     <strong>Quantity :</strong> {eachCake.quantity}
                //                                                 </div>
                //                                                 <div style={{ textAlign: 'left' }}>
                //                                                     <strong>Weight :</strong> {eachCake.weight}
                //                                                 </div>
                //                                             </div>
                //                                         </div>
                //                                     )
                //                                 })
                //                             }
                //                         </div>
                //                     </div>
                //                 </>
                //             )
                //         })
                //     }
                // </div>
            }
            {orders?.length === 0 && <div className="accordion" id="accordionExample">
                No Orders Found!!
            </div>}
        </div>
    )
}

export default Orders