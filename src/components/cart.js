import axios from "axios";
import { useEffect, useState } from 'react';
import { connect } from "react-redux";
import { Link, withRouter } from 'react-router-dom';


function Cart(props) {
    var [cart, setCart] = useState([]);
    var totalprice = 0;

    useEffect(() => {
        axios({
            url: process.env.REACT_APP_BASE_URL+ "/cakecart",
            method: 'post',
            data: {
            },
        }).then((response) => {
            var allcart = response.data.data;
            setCart(allcart);
        }, (error) => {
            alert("Something went wrong try again later")
        })
    }, [])

    let emptyCart = () => {
        axios({
            url: process.env.REACT_APP_BASE_URL+ "/clearcart",
            method: 'post',
            data: {
            },
        }).then((response) => {
            setCart([]);
            props.dispatch({
                type:"EMPTYCART"
            })
            props.history.push("/")
        }, (error) => {
            alert("Something went wrong try again later")
        })
    };

    let removeFromCart = (event) => {
        var cakeid = event.target.dataset.remove
        var index = event.target.dataset.index
        var price = event.target.dataset.price
        axios({
            url: process.env.REACT_APP_BASE_URL+ "/removecakefromcart ",
            method: 'post',
            data: {
                "cakeid":cakeid
            },
        }).then((response) => {
            console.log(response)
            console.log(cakeid)
            console.log(index)
            props.dispatch({
                type:"REMOVEFROMCART",
                payload:{
                    "index":index,
                    "price":price
                }
            })
        }, (error) => {
            alert("Something went wrong try again later")
        })
    };

    let removeOneFromCart = (cakeid) => {
        console.log(cakeid)
        axios({
            url: process.env.REACT_APP_BASE_URL+ "/removeonecakefromcart ",
            method: 'post',
            data: {
                "cakeid":cakeid
            },
        }).then((response) => {
            console.log(response)
            props.dispatch({
                type:"REMOVEONEFROMCART",
            })
        }, (error) => {
            alert("Something went wrong try again later")
        })
    };

    let addtocart = (data) => {
        axios({
            url: process.env.REACT_APP_BASE_URL+ "/addcaketocart",
            method: 'post',
            data: data,
        }).then((response) => {
            props.dispatch({
                type:"ADDTOCART",
                payload:{
                    "data":response.data.data
                }
            })
            window.location.reload()
        }, (error) => {
            alert("Something went wrong try again later")
        })
    }

    return (
        <>
            <div className="container">
                <table id="cart" className="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th style={{width:"60%"}}>Product</th>
                            <th style={{width:"10%"}}>Price</th>
                            <th style={{width:"10%"}}>Quantity</th>
                            <th style={{width:"10%", textAlign:"center"}}></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            (cart).map((each, index)=>{
                                totalprice += each.price
                                return (
                                <tr key={index}>
                                    <td data-th="Product">
                                        <div className="row">
                                            <div className="col-sm-2 hidden-xs"><img src={each.image} alt="..." className="img-responsive" /></div>
                                            <div className="col-sm-10">
                                                <h4 className="nomargin">{each.name}</h4>
                                            </div>
                                        </div>
                                    </td>
                                    <td data-th="Price">${each.price}</td>
                                    <td data-th="Quantity">
                                        <button onClick={() => removeOneFromCart(each.cakeid)} className="btn btn-primary"><i className="fa fa-minus"></i></button> 
                                        <input type="number" className="form-control text-center" value={each.quantity} />
                                        <button onClick={() => addtocart(each)} className="btn btn-primary"><i className="fa fa-plus"></i></button>
                                     </td>
                                    <td className="actions" data-th="">
                                        <button data-remove={each.cakeid} data-index={index} data-price={each.price} onClick={removeFromCart} className="btn btn-danger btn-sm"><i className="fa fa-trash-o"></i></button>
                                    </td>
                                </tr>
                                )
                            })
                        }
                    </tbody>
                    <tfoot>
                        <tr>
                            <td><button onClick={emptyCart} className="btn btn-warning"><i className="fa fa-trash-o"></i> Empty Cart</button></td>
                            <td className="hidden-xs text-center"><strong>Total ${totalprice}</strong></td>
                            <td className="hidden-xs"></td>
                            <td><Link to="/checkout" className="btn btn-success btn-block">Checkout <i className="fa fa-angle-right"></i></Link></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </>
    );
}

Cart = connect((state, props) => {
    if(state.CartReducer?.remove==true){
        state.CartReducer.remove=false;
        window.location.reload()
    }
})(Cart)

export default withRouter(Cart)