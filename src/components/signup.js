import {Component} from "react";
import axios from "axios";
import { Link } from "react-router-dom";

class Signup extends Component {
    constructor() {
        super();
        this.state = {
            name:'',
            email: '',
            password: '',
            errors: {
                name:'',
                email: '',
                password: ''
            }
        }
    }

    handleChange = (event) => {
        event.preventDefault();
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const { name, value } = event.target;
        let errors = this.state.errors;

        switch (name) {
            case 'name':
                errors.email = value.length < 1?'Name cannot not be empty':'';
                break;
            case 'email':
                errors.email = re.test(value)?'':'Email is not valid!';
                break;
            case 'password':
                errors.password = value.length < 1?'Password cannot not be empty':'';
                break;
            default:
                break;
        }

        this.setState({errors, [name]: value});
    }

    handleSubmit = (event) => {
        event.preventDefault();
        axios({
            url: process.env.REACT_APP_BASE_URL+ "/register",
            method: 'post',
            data: {name:this.state.name, email: this.state.email, password: this.state.password}
        }).then(res => {
            this.props.history.push('/signin')
        }, err => {})
    }

    render() {
        const {errors} = this.state;
        return (
            <div className="container" style={{marginTop: "100px", marginBottom: "100px"}}>
                <div className="row">
                    <div className="col-6 text-left"><label>Sign up</label> </div>
                    <div className="col-6 text-right"><Link to="/signin" > <label>Sign in</label></Link></div>
                </div>
                <form onSubmit={this.handleSubmit} noValidate>
                    <div className="form-group name">
                        <input value={this.state.name} name='name' onChange={this.handleChange} className="form-control" placeholder="Enter Name"/>
                        {errors.name.length > 0 && <span className='error'>{errors.name}</span>}
                    </div>
                    <div className="form-group email">
                        <input value={this.state.email} name='email' onChange={this.handleChange} className="form-control" placeholder="Enter Email ID"/>
                        {errors.email.length > 0 && <span className='error'>{errors.email}</span>}
                    </div>
                    <div className="form-group role">
                        <input type="password" value={this.state.password} name='password' onChange={this.handleChange} className="form-control" placeholder="Enter Password"/>
                        {errors.password.length > 0 && <span className='error'>{errors.password}</span>}
                    </div>
                    <button type="submit" className="form-control btn btn-primary">Sign Up</button>
                </form>
            </div>
        )
    }
}

export default Signup;