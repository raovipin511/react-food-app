import Navbar from './components/navbar';
import Home from './components/home';
import Signin from './components/signin';
import Signup from './components/signup';
import Pagenotfound from './components/pagenotfound';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Search from './components/search';
import Productdetail from './components/productdetail';
import Cart from './components/cart';
import Checkout from './components/checkout';
import axios from 'axios';
import Orders from './components/orders';
import admin from './components/admin';
import addcake from './components/addcake';


function App() {

  axios.interceptors.request.use((request)=>{
    request.headers["authtoken"] = localStorage.token
    return request;
  }, (error)=>{
    return Promise.reject(error)
  })

  return (
    <>
      <Router>
        <Navbar ></Navbar>
        <Switch>
          <Route exact path="/"><Home /> </Route>
          <Route exact path="/signin"> <Signin /> </Route>
          <Route exact path="/signup" component={Signup} />
          <Route exact path="/search" component={Search} />
          <Route exact path="/product/:proId" component={Productdetail} />
          <Route exact path="/cart" component={Cart} />
          <Route path="/checkout" component={Checkout} />
          <Route exact path="/orders" component={Orders} />
          <Route exact path="/admin/cake" component={admin} />
          <Route exact path="/admin/addcake" component={addcake} />
          <Route path="/*" component={Pagenotfound} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
