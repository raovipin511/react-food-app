import axios from "axios";


export function loginmiddleware(data){
    return function(dispatch){
        axios({
            url: process.env.REACT_APP_BASE_URL+ "/login",
            method: 'post',
            data: {email: data.email, password: data.password}
        }).then(res => {
            dispatch({
                type:"LOGIN_START",
            })
            if(res.message!="Invalid Credentials"){
                localStorage.token = res.data.token
                localStorage.username = res.data.name
                localStorage.email = res.data.email
                dispatch({
                    type:"LOGIN_SUCCESS",
                    payload:{
                        token:res.data.token,
                        username:res.data.name
                    }
                })
            }else{
                alert("Invalid Credentials")
            }
        }, err => {
            dispatch({
                type:"LOGIN_FAIL",
            })
        })
    }
}
