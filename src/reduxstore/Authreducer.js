function AuthReducer(state={
  token : localStorage.token,
  isLoggedIn : localStorage.token?true:false,
  username : undefined,
  isLoading : false
}, action) {
    switch(action.type) {
        case "LOGIN_START" : {
            state = {...state}
            state["isLoading"] = true
            return state
        }
        case "LOGIN_SUCCESS" : {
            state = {...state}
            state["token"] = action.payload?.token
            state["username"] = action.payload?.username
            state["isLoggedIn"] = true
            state["isLoading"] = false
            return state
        }
        case "LOGIN_FAIL" : {
            state = {...state}
            state["token"] = action.payload?.token
            state["username"] = action.payload?.username
            state["isLoggedIn"] = false
            state["isLoading"] = false
            return state
        }
        case "LOGOUT" : {
            state = {...state}
            localStorage.clear()
            state["token"] = ""
            state["username"] = ""
            state["isLoggedIn"] = false
            state["isLoading"] = false
            return state
        }
        default :
        return state
    }
}
export default AuthReducer