function CartReducer(state = {
    cart:[],
    totalprice:0,
    added:false,
    remove:false,
    success: false
}, action) {
    switch (action.type) {
        case "ADDTOCART": {
            state = { ...state }
            console.log(action.payload)
            state.cart.push(action.payload.data)
            state["totalprice"] += action.payload.data.price
            state["added"] = true
            state["remove"] = false
            return state
        }
        case "EMPTYCART": {
            state = { ...state }
            state.cart=[]
            state["totalprice"] = 0
            state["added"] = false
            state["remove"] = true
            return state
        }
        case "REMOVEFROMCART": {
            state = { ...state }
            state.cart.splice(action.payload.index, 1);
            state["totalprice"] = state["totalprice"]-action.payload.price
            state["added"] = false
            state["remove"] = true
            return state
        }
        case "REMOVEONEFROMCART": {
            state = { ...state }
            state["added"] = false
            state["remove"] = true
            return state
        }
        case "PLACEORDER" : {
            state = {...state}
            state["success"] = true
            return state
        }
        default:
            return state
    }
}
export default CartReducer