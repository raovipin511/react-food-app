import {applyMiddleware, combineReducers, createStore} from "redux"
import AuthReducer from "./Authreducer";
import CartReducer from "./Cartreducer";
import thunk from "redux-thunk";
import createSaga from "redux-saga"
import MainSaga from "./sagas";

let middle = state=>next=>action=>{
    // alert(JSON.stringify(action))
    console.log(action.type + " action dispatched at "  );
    next(action)
}

var SagaMiddleware = createSaga()
var reducers = combineReducers({AuthReducer,CartReducer})

let store = createStore(reducers, applyMiddleware(middle, thunk, SagaMiddleware))
SagaMiddleware.run(MainSaga)

export default store